FROM python:3
LABEL maintainer="peter.bourgonje@dfki.de"


RUN apt-get -y update && \
    apt-get upgrade -y && \
    apt-get install -y python3-dev &&\
    apt-get update -y


ADD requirements.txt .
RUN pip3 install -r requirements.txt
RUN python3 -m nltk.downloader punkt

ADD bert-base-multilingual-cased /custombert/bert-base-multilingual-cased
ADD nermodels/wikiner-nl nermodels/wikiner-nl
ADD bert.py .
ADD ner.py .
ADD nif.py .
ADD utils.py .
ADD flaskController.py .
ADD config.ini .

RUN export LC_ALL=C.UTF-8
RUN export LANG=C.UTF-8

EXPOSE 8080

ENTRYPOINT FLASK_APP=flaskController.py flask run --host=0.0.0.0 --port=8080
#CMD ["/bin/bash"]
