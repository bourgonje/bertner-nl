Service for entity spotting using bert and trained on the wikiner corpus.
This instance is for Dutch only (took out the language parameter in the flask controller, and links in a hardcoded way to nermodels/wikiner-nl for the trained/tuned bert model).
